#!/usr/bin/python

import urllib
import sys
import re


def get_urls(page):

    ''' Gets all of the urls from a webpage '''

    i = 0
    while i < len(page):
        # Find the beginning of the tag
        if page[i:(i+7)] == '<a href':
            tag_begin = i
            # Find the end of the tag
            j = i
            while j < len(page):
                if page[j] == '>':
                    tag_end = j
                    break
                j += 1
            # Define the tag
            tag = page[tag_begin:tag_end]

            # Get everything between the quotes
            # Find the beginning of the url
            k = 0
            while k < len(tag):
                if tag[k] == '\"':
                    url_begin = k+1
                    # Find the end of the url
                    l = url_begin
                    while l < len(tag):
                        if tag[l] == '\"':
                            url_end = l
                            break
                        l += 1
                    # Define the url
                    url = tag[url_begin:url_end]
                    #  Add the urls to the list
                    print url
                k += 1
        i += 1


def validate_url(url):
    # Logic to define a url
    urlpattern = r'^[http]{4}'

    if re.match(urlpattern, url):
        return True


def main(url):
    # URL is specified as an argument on the command line.
    if validate_url(url):
        page = urllib.urlopen(url)

        # Read the lines and extract the URLS
        for line in page.readlines():
            # Print the URLS only
            print get_urls(line)

        # Close the page
        page.close()
    else:
        print 'invalid url'


# For testing: http://www.csparks.com/XMLWithoutTears/
url = sys.argv[1]

if __name__ == '__main__':
    main(url)
