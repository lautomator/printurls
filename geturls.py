#!/usr/bin/python

import urllib2
import re
import sys


def geturls(page):

    url = r'"((http|ftp)s?://.*?)"'
    urls = re.findall(url, page)

    return urls


def main(target):

    # If a connection is made,
    # open a stream to a webpage.
    try:
        source = urllib2.urlopen(target)
        page = source.read()
        source.close()
    except Exception:
        print target, 'is an invalid URL.'
        print 'usage: python [this-script] <http(s)://valid-url>'
        exit()

    result = geturls(page)

    # Display reader-friendly output.
    for link in result:
        print link


# For testing: http://www.csparks.com/XMLWithoutTears/
# target = 'http://www.csparks.com/XMLWithoutTears/'
try:
    target = sys.argv[1]
    if __name__ == '__main__':
        main(target)
except Exception, e:
    print 'get_urls requires one argument:'
    print 'usage: python [this-script] <http(s)://valid-url>'
